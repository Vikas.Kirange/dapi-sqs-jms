package com.ally.poc;

import java.util.Arrays;

import org.apache.camel.CamelContext;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.impl.DefaultCamelContext;
import org.apache.camel.impl.SimpleRegistry;
import org.apache.camel.test.junit4.CamelTestSupport;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.sqs.AmazonSQS;
import com.amazonaws.services.sqs.AmazonSQSClientBuilder;

public class TextMessageSenderWithCamelComponent extends CamelTestSupport {

	public static void main(String args[]) throws Exception {

		ExampleConfiguration config = ExampleConfiguration.parseConfig("TextMessageSender", args);
		AmazonSQS client = AmazonSQSClientBuilder.standard().withCredentials(config.getCredentialsProvider())
				.withRegion(Regions.US_EAST_1).build();

		SimpleRegistry registry = new SimpleRegistry();
		registry.putIfAbsent("client", client);

		System.out.println("connectionFactory  is ..." + client.toString() + " " + client.listQueues());
		try {
			CamelContext context = new DefaultCamelContext(registry);
			System.out.println("Camel context is .." + context.getComponentNames().toString());
			System.out.println("Endpoint size is .." + context.getEndpoints().size());
			System.out.println("Endpoint context is .." + context.getEndpoints().toString());
			System.out.println("Endpoint is .." + Arrays.toString(context.getEndpoints().toArray()));

			TextMessageSenderWithCamelComponent tx = new TextMessageSenderWithCamelComponent();
			context.addRoutes(tx.createRouteBuilder());
			context.start();
			System.out.println("Context started in main....");
			Thread.sleep(10000);
			context.stop();
			System.out.println("Camel published message on aws queue using jms component....");
		} catch (Exception e) {
			System.out.println("Exception is ..." + e.getMessage());
			e.printStackTrace();
		}
	}

	@Override
	protected RouteBuilder createRouteBuilder() throws Exception {
		return new RouteBuilder() {
			@Override
			public void configure() {
				System.out.println("Inside configure....");
				onException(Exception.class).process(new FailureProcessor()).log("Received body ... ").handled(true).maximumRedeliveries(1);

				// from("file:data/inbox?noop=true").to("log:?level=DEBUG&showBody=true&showHeaders=true").to("jms:dapi-aws-sqs-poc").to("mock:end");
				from("file:data/inbox?noop=true").to("log:?level=DEBUG&showBody=true&showHeaders=true")
						.to("aws-sqs:dapi-aws-sqs-poc?amazonSQSClient=#client&defaultVisibilityTimeout=5000&deleteAfterRead=false").throwException(new Exception("..ppppublisheed"))
						.to("mock:end");
				System.out.println("After route configure....");
			}

		};
	}

	// @Test
	// public void testCamelRoute() throws Exception {
	// getMockEndpoint("mock:end").expectedMessageCount(1);
	// assertMockEndpointsSatisfied();
	// }
	//
	// public Object generateBody() {
	// String bodyMessage = "{" + "\"type\": \"key1\"," + "\"value\":\"val1\"" +
	// "}";
	// return bodyMessage;
	// }
}