package com.ally.poc;

import java.util.Arrays;

import javax.jms.ConnectionFactory;
import org.apache.camel.CamelContext;
import org.apache.camel.Endpoint;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.component.jms.JmsComponent;
import org.apache.camel.impl.DefaultCamelContext;
import org.apache.camel.test.junit4.CamelTestSupport;
import org.junit.Test;
import org.apache.camel.component.jms.JmsConfiguration;
import org.apache.camel.component.jms.JmsEndpoint;
import org.apache.camel.component.jms.JmsQueueEndpoint;

import com.amazon.sqs.javamessaging.ProviderConfiguration;
import com.amazon.sqs.javamessaging.SQSConnectionFactory;
import com.amazonaws.services.sqs.AmazonSQS;
import com.amazonaws.services.sqs.AmazonSQSClientBuilder;

public class TextMessageSenderWithCamel extends CamelTestSupport {

	public static void main(String args[]) throws Exception {

		ExampleConfiguration config = ExampleConfiguration.parseConfig("TextMessageSender", args);

		// Create the connection factory based on the config
		ConnectionFactory connectionFactory = new SQSConnectionFactory(new ProviderConfiguration(),
				AmazonSQSClientBuilder.standard().withRegion(config.getRegion().getName())
						.withCredentials(config.getCredentialsProvider()));

		// AmazonSQS client = AmazonSQSClientBuilder.standard().build();

		System.out.println("connectionFactory  is ..." + connectionFactory.toString() + " "
				+ connectionFactory.getClass().getName());

		String sqsQueueName = "dapi-aws-sqs-poc";
		String sqsQueueEndpoint = "https://sqs.us-east-1.amazonaws.com/650487181597/";

		try {
			JmsConfiguration configuration = new JmsConfiguration(connectionFactory);
			CamelContext context = new DefaultCamelContext();
			JmsComponent jmsComponent = new JmsComponent(context);
			jmsComponent.setConfiguration(configuration);
			JmsQueueEndpoint jmsQueueEndpoint = (JmsQueueEndpoint) jmsComponent.createEndpoint(sqsQueueEndpoint);

			context.addComponent("jms", jmsComponent);
			context.addEndpoint("jmsQueueEndpoint", jmsQueueEndpoint);

			System.out.println("Camel context is .." + context.getComponentNames().toString());
			System.out.println("Endpoint size is .." + context.getEndpoints().size());
			System.out.println("Endpoint context is .." + context.getEndpoints().toString());
			System.out.println("jmsQueueEndpoint.getDestinationName() is : - " + jmsQueueEndpoint.getDestinationName());
			System.out.println("Endpoint is .." + Arrays.toString(context.getEndpoints().toArray()));

			TextMessageSenderWithCamel tx = new TextMessageSenderWithCamel();
			context.addRoutes(tx.createRouteBuilder());
			context.start();
			System.out.println("Context started in main....");
			Thread.sleep(10000);
			context.stop();
			System.out.println("Camel published message on aws queue using jms component....");
		} catch (Exception e) {
			System.out.println("Exception is ..." + e.getMessage());
			e.printStackTrace();
		}
	}

	@Override
	protected RouteBuilder createRouteBuilder() throws Exception {
		return new RouteBuilder() {
			@Override
			public void configure() {
				System.out.println("Inside configure....");
				// from("direct:start").setBody().method(this.generateBody()).to(jmsComponent.createEndpoint(sqsQueueEndpoint));
				// from("direct:start").setBody().method(this.generateBody()).to("jms:jmsQueueEndpoint");
				// from("direct:start").setBody().method(this.generateBody()).to("jms:https://sqs.us-east-1.amazonaws.com/650487181597/dapi-aws-sqs-poc");
				// from("direct:start").setBody().method(this.generateBody()).to("jms:dapi-aws-sqs-poc");
				// from("direct:start").setBody().constant("This is from test
				// code...").log("After From ...")
				// .to("jms:arn:aws:sqs:us-east-1:650487181597:dapi-aws-sqs-poc").log("After
				// to..");
				// from("file:data/inbox?noop=true").to("stream:out").to("mock:end");
				// errorHandler(deadLetterChannel("mock:end").logStackTrace(true).useOriginalMessage().onPrepareFailure(new
				// FailureProcessor()));

				// from("file:data/inbox?noop=true").to("log:?level=DEBUG&showBody=true&showHeaders=true").to("jms:jmsQueueEndpoint").to("mock:end");

				onException(Exception.class).process(new Processor() {
					public void process(Exchange exchange) throws Exception {
						System.out.println("In hadling exception..." + exchange.getMessage());
					}
				}).log("Received body ... ").handled(true).maximumRedeliveries(1);

				// from("file:data/inbox?noop=true").to("log:?level=DEBUG&showBody=true&showHeaders=true").to("jms:dapi-aws-sqs-poc").to("mock:end");
				from("file:data/inbox?noop=true").to("log:?level=DEBUG&showBody=true&showHeaders=true")
						.to("jms:dapi-aws-sqs-poc").to("mock:end");
				System.out.println("After route configure....");
			}

		};
	}

	@Test
	public void testCamelRoute() throws Exception {
		getMockEndpoint("mock:end").expectedMessageCount(1);
		assertMockEndpointsSatisfied();
	}

	public Object generateBody() {
		String bodyMessage = "{" + "\"type\": \"key1\"," + "\"value\":\"val1\"" + "}";
		return bodyMessage;
	}
}